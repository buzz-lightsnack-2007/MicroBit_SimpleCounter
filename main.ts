/** 
    CounterTracker
    simple counter tracker for the Micro:Bit

 */

input.onButtonPressed(Button.A, function () {
    count += 1;
    basic.showNumber(count)
})

input.onButtonPressed(Button.AB, function() {
    count = 0;
    basic.showNumber(count)
})

function startup(): boolean {
    music.ringTone(Note.C)
    count = 0;
    basic.showNumber(count)
    return true
}

let count = 0;
startup()